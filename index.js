const token = "87c14f44261a0e1c7fbf86ee9f764189b28fc99f10d09442c47f74ddafc29774"
const key = "ad0b9ac59525d9c9303101674efb30d8"

const btn = document.getElementById('addCard')
const card = document.getElementById('card')
const listContainer = document.getElementById('listCContainer')
const cardsContainer = document.querySelector('.cardscontainer')
const showmodal = document.getElementById('myModal')

const deleteChecklist = document.getElementsByClassName('checklist-container')
const addchecklist = document.getElementById('addchecklist')
const checklistName = document.getElementById('checklist-name')
const iteminput = document.getElementsByClassName('checkitem-name-input')
const additembtn = document.getElementsByClassName('checkitems-add-button')
const items = document.getElementsByClassName('checkitems')
const modalbody = document.getElementsByClassName('modal-body')
const showcard=document.getElementsByClassName('showcard-addbutton');
const cardInputContainer=document.getElementsByClassName('card-input-container')
const closeCard=document.getElementsByClassName('closecard')

btn.addEventListener('click', addCardHandler)
cardsContainer.addEventListener('click', removeItem)
addchecklist.addEventListener('click', createchecklist)
modalbody[0].addEventListener('click', deletechecklist)
showcard[0].addEventListener('click',showcardInputDialogue)
closeCard[0].addEventListener('click',closeCardInput)

//disable enter key 
window.addEventListener('keydown',function(e){if(e.keyIdentifier=='U+000A'||e.keyIdentifier=='Enter'||e.keyCode==13){if(e.target.nodeName=='INPUT'&&e.target.type=='text'){e.preventDefault();return false;}}},true);

// show input box for add new card 
function showcardInputDialogue(event){
    cardInputContainer[0].style.display='block'
    showcard[0].style.display='none'
}

// close the input box 
function closeCardInput(){
    cardInputContainer[0].style.display='none'
    showcard[0].style.display='block'
}


// display cards in a list
displaycard()
function displaycard () {
  let url =
    `https://api.trello.com/1/lists/5e88624a9c3d75189309b2eb/cards?key=${key}&token=${token}`
  fetch(url)
    .then(response => response.json())
    .then(cards => {
      cards.forEach(card => {
        // console.log(card.name)
        renderCard(card.name, card.id)
      })
    })
}
// add a card to the api and show card in the browser
function addCard (cardname) {
  let url = `https://api.trello.com/1/cards?idList=5e88624a9c3d75189309b2eb&key=${key}&token=${token}`
  fetch(url, {
    method: 'POST',
    body: JSON.stringify({
      name: cardname
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then(response => response.json())
    .then(cards => {
      console.log(cards)
      renderCard(cards.name, cards.id)
    })
}

// add card and delete card button  to the container
function renderCard (cardName, cardId) {
  //  console.log("input getting"+card.value)
  let newDiv = document.createElement('div')
  newDiv.classList.add('bg-light')
  newDiv.classList.add('mb-1')
  newDiv.classList.add('pb-2')
  newDiv.classList.add('pt-2')
  newDiv.classList.add('showchecklist')
  newDiv.setAttribute('data-cardId', cardId)
  newDiv.setAttribute('data-cardName', cardName)

  newDiv.appendChild(document.createTextNode(cardName))

  var deleteBtn = document.createElement('button')

  deleteBtn.className = 'btn btn-danger btn-sm float-right delete'

  deleteBtn.appendChild(document.createTextNode('X'))
  newDiv.appendChild(deleteBtn)
  cardsContainer.appendChild(newDiv)
}

// add card to the trello api and  render card
function addCardHandler() {
  addCard(card.value)
}

function addChecklist (checklistName, cardid) {
  let url = `https://api.trello.com/1/checklists?idCard=${cardid}&key=${key}&token=${token}`
  fetch(url, {
    method: 'POST',
    body: JSON.stringify({
      name: checklistName
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then(response => response.json())
    .then(checklist => {
      console.log('post request response')
      console.log(checklist)
      renderChecklist(checklist.name, checklist.id)
    })
}

function dispalychecklist (cardid) {
  let url = `https://api.trello.com/1/cards/${cardid}/checklists?key=${key}&token=${token}`
  fetch(url)
    .then(response => response.json())
    .then(cards => {
      console.log(cards)
      cards.forEach(checklistItr => {
        console.log(checklistItr.name)
        console.log(checklistItr.id)

        renderChecklist(checklistItr.name, checklistItr.id)
      })
    })
}

function createchecklist () {
  addChecklist(checklistName.value, showmodal.getAttribute('data-card-id'))
}

function deletechecklistrequest (checklistid, li) {
  const checklist = document.getElementById('checklists-container')

  fetch(
    `https://api.trello.com/1/checklists/${checklistid}?key=${key}&token=${token}`,
    {
      method: `DELETE`
    }
  )
    .then(e => {
      checklist.removeChild(li)
    })
    .catch(e => console.log(e + ' Not deleted'))
}

// selector event for dletechecklist,add checklistItem, delete checklistItem and updateitem 
function deletechecklist (e) {
  if (e.target.classList.contains('delete-checklist')) {
    if (confirm('Are You Sure?')) {
      var li = e.target.parentElement.parentElement.parentElement
      let checklistId = e.target.parentElement.getAttribute('data-checklist-id')
      console.log(checklistId)
      deletechecklistrequest(checklistId, li)
    }
  }
  if (event.target.classList.contains('checkitems-add-button')) {
    console.log('chceckbuutton clicked')

    let checkitemName =
      event.target.previousElementSibling.firstElementChild.value

    let checklistId = event.target.parentElement.previousElementSibling.firstElementChild.getAttribute(
      'data-checklist-id'
    )
    console.log(checklistId)
    console.log(checkitemName)

    let positionOfAppendChild =
      event.target.parentElement.previousElementSibling.lastElementChild
    addChecklistItemRequest(checklistId, checkitemName, positionOfAppendChild)
  }
  if (event.target.classList.contains('delete-checkitem')) {
    console.log('you clicked on delete button')
    let deletediv = event.target.parentElement
    console.log(deletediv)
    let idCheckItem = event.target.parentElement.getAttribute(
      'data-checklistitem-id'
    )
    let idChecklist = event.target.parentElement.parentElement.previousElementSibling.getAttribute(
      'data-checklist-id'
    )
    console.log('id checklist' + idChecklist)
    console.log('id checkitem' + idCheckItem)

    deleteCheckitem(idCheckItem, idChecklist, deletediv)
  }
  if (event.target.classList.contains('update-item')) {
    updateCheckItems(event)
  }
}

// getting card id and delete card from a list
function removeItem (e) {
    if (event.target.classList.contains('showchecklist')) {
      let modal = document.querySelector('.modal')
      let closeButton = document.querySelector('.close')
  
      showmodal.setAttribute(
        'data-card-id',
        event.target.getAttribute('data-cardid')
      )
  
      let newdiv = document.createElement('div')
      newdiv.setAttribute('id', 'checklists-container')
      modalbody[0].appendChild(newdiv)
      dispalychecklist(event.target.getAttribute('data-cardid'))
      $('#myModal').modal('show')
      const checklist = document.getElementById('checklists-container')
  
      closeButton.onclick = function () {
        modal.style.display = 'none'
        checklist.remove()
      }
  
      window.onclick = function (event) {
        if (event.target == modal) {
          modal.style.display = 'none'
          checklist.remove()
        }
      }
    }
    if (e.target.classList.contains('delete')) {
      if (confirm('Are You Sure?')) {
        var li = e.target.parentElement
        let cardId=e.target.parentElement.getAttribute('data-cardid')
                fetch(
                  `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`,
                  {
                    method: `DELETE`
                  }
                )
                  .then(e => {
                    cardsContainer.removeChild(li)
                  })
                  .catch(e => console.log(e + ' Not deleted'))
         }
    }
  }
  
function renderChecklist (checklistName, checklistId) {
  const checklist = document.getElementById('checklists-container')
  //div for whole checklist
  let checklistDiv = document.createElement('div')
  checklistDiv.classList.add('single-checklist-container')

  let newDiv = document.createElement('div')
  newDiv.classList.add('checklist')
  newDiv.classList.add('d-flex')
  newDiv.classList.add('justify-content-between')
  newDiv.setAttribute('data-checklist-id', checklistId)
  newDiv.classList.add('mb-2')
  let newpTag = document.createElement('p')
  newpTag.innerHTML = `${checklistName}`
  let newBtn = document.createElement('button')
  newBtn.className = 'text-white btn-dark btn delete-checklist'
  newBtn.appendChild(document.createTextNode('delete'))
  newDiv.appendChild(newpTag)
  newDiv.appendChild(newBtn)
  //checklist.appendChild(newDiv)

  let outerDiv = document.createElement('div')
  outerDiv.classList.add('checklist-container')
  // outerDiv.appendChild(newDiv)
  checklistDiv.appendChild(outerDiv)
  outerDiv.appendChild(newDiv)

  let checkItemsContainer = document.createElement('div')
  checkItemsContainer.classList.add('checkitems')
  checkItemsContainer.classList.add(checklistId)

  outerDiv.appendChild(checkItemsContainer)
  checklistDiv.appendChild(outerDiv)

  let addCheckItemContainer = document.createElement('div')
  let newForm = document.createElement('form')
  let newInput = document.createElement('input')
  newInput.classList.add('form-control')
  newInput.classList.add('mb-3')
  newInput.classList.add('checkitem-name-input')

  newInput.setAttribute('type', 'text')
  newInput.setAttribute('placeholder', 'checkitems-name')
  newInput.setAttribute("onfocus","this.value=''")

  newForm.appendChild(newInput)
  let addCheckitembtn = document.createElement('btn')
  addCheckitembtn.className = 'text-white btn-dark btn checkitems-add-button'
  addCheckitembtn.appendChild(document.createTextNode('add item'))
  addCheckItemContainer.appendChild(newForm)
  addCheckItemContainer.appendChild(addCheckitembtn)

  checklistDiv.appendChild(addCheckItemContainer)
  console.log(checklist)

  checklist.appendChild(checklistDiv)

  getCheckitems(checklistId)
}
function getCheckitems (idchecklist) {
  let url1 = `https://api.trello.com/1/checklists/${idchecklist}/checkItems?key=${key}&token=${token}`
  fetch(url1)
    .then(response => response.json())
    .then(checkItems => {
      console.log(idchecklist)
      console.log('checekitems in a checklist')
      console.log(checkItems)
      checkItems.forEach(checkItem => {
        console.log(checkItem.name)
        console.log(checkItem.id)
        createCheckitem(checkItem.name, checkItem.id, idchecklist)
      })
    })
}

function createCheckitem (checklistItemname, checklistItemid, idchecklist) {
  console.log('checkbutton clicked')
  let newDiv = document.createElement('div')
  newDiv.classList.add('bg-light')
  newDiv.classList.add('mb-1')
  newDiv.classList.add('pb-2')
  newDiv.classList.add('pt-2')
  newDiv.classList.add('update-item')
  newDiv.setAttribute('data-checklistItem-id', checklistItemid)

  newDiv.appendChild(document.createTextNode(checklistItemname))

  var deleteBtn = document.createElement('button')

  deleteBtn.className = 'btn btn-danger btn-sm float-right delete-checkitem'

  deleteBtn.appendChild(document.createTextNode('X'))
  newDiv.appendChild(deleteBtn)
  let item = document.getElementsByClassName(`${idchecklist}`)
  item[0].appendChild(newDiv)
}

function deleteCheckitem (idCheckItem, idChecklist, deletediv) {
  fetch(
    `https://api.trello.com/1/checklists/${idChecklist}/checkItems/${idCheckItem}?key=${key}&token=${token}`,
    {
      method: `DELETE`
    }
  )
    .then(e => {
      items[0].removeChild(deletediv)
    })
    .catch(e => console.log(e + ' check item Not deleted'))
}

function updateCheckItems (event) {
  let cardId = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getAttribute(
    'data-card-id'
  )

  let checkItemId = event.target.getAttribute('data-checklistitem-id')

  const newItemName = prompt('Enter Item Name')
  console.log(cardId)
  console.log(checkItemId)
  console.log(newItemName)

  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?name=${newItemName}&key=${key}&token=${token}`,
    {
      method: 'PUT'
    }
  )
    .then(data => data.json())
    .then(data => {
      var item = event.target.childNodes[0]
      console.log(event.target.childNodes)
      item.nodeValue = newItemName
    })
}

function addChecklistItemRequest (
  checklistId,
  checkitemName,
  positionOfAppendChild
) {
  console.log(checklistId)
  let url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`
  fetch(url, {
    method: 'POST',
    body: JSON.stringify({
      name: checkitemName
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then(response => response.json())
    .then(checklistItemResponse => {
      console.log('post request response of add checkitem')
      console.log(checklistItemResponse)
      addCheckItemOnClickOfButton(
        checklistItemResponse.name,
        checklistItemResponse.id,
        positionOfAppendChild,
        checklistId
      )
    })
}

function addCheckItemOnClickOfButton (
  checklistItemName,
  checklistItemId,
  positionOfAppendChild,
  checklistId
) {
  let newDiv = document.createElement('div')
  newDiv.classList.add('bg-light')
  newDiv.classList.add('mb-1')
  newDiv.classList.add('pb-2')
  newDiv.classList.add('pt-2')
  newDiv.classList.add('update-item')
  newDiv.setAttribute('data-checklistItem-id', checklistItemId)

  newDiv.appendChild(document.createTextNode(checklistItemName))

  var deleteBtn = document.createElement('button')

  deleteBtn.className = 'btn btn-danger btn-sm float-right delete-checkitem'

  deleteBtn.appendChild(document.createTextNode('X'))
  newDiv.appendChild(deleteBtn)
  let item = document.getElementsByClassName(`${checklistId}`)
  positionOfAppendChild.appendChild(newDiv)
}

